package com.itau.controllers;

import com.itau.models.DTO.PagamentoDTO;
import com.itau.models.Transacao;
import com.itau.services.TransacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    TransacaoService transacaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Transacao efetuaPagamento(@RequestBody PagamentoDTO pagamentoDTO){
        Transacao transacao = new Transacao();

        transacao.setIdCartao(pagamentoDTO.getIdCartao());
        transacao.setDescricaoTransacao(pagamentoDTO.getDescricao());
        transacao.setValorTransacao(pagamentoDTO.getValor());
        return transacaoService.salvarTransacao(transacao);

    }

}

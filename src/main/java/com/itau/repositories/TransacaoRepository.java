package com.itau.repositories;

import com.itau.models.Transacao;
import org.springframework.data.repository.CrudRepository;

public interface TransacaoRepository extends CrudRepository<Transacao, Integer> {
}

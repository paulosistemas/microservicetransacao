package com.itau.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cartao utilizado no pagamento nao foi encontrado.")
public class CartaoNotFoundException extends RuntimeException{
}

package com.itau.clients;

import com.itau.models.Transacao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao")
public interface CartaoClient {

    @GetMapping("/cartao/{idCartao}")
    Transacao getClienteById(@PathVariable int idCartao);

}

package com.itau.services;

import com.itau.clients.CartaoClient;
import com.itau.exceptions.CartaoNotFoundException;
import com.itau.models.Transacao;
import com.itau.repositories.TransacaoRepository;
import com.netflix.discovery.converters.Auto;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransacaoService {

    @Autowired
    TransacaoRepository transacaoRepository;

    @Autowired
    CartaoClient cartaoClient;

    public Transacao salvarTransacao(Transacao transacao){

        validarCartao(transacao.getIdCartao());
        return transacaoRepository.save(transacao);
    }

    public void validarCartao(int idCartao){
        try {
            cartaoClient.getClienteById(idCartao);
        }catch (FeignException.BadRequest e){
            throw new CartaoNotFoundException();
        }
    }
}
